<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Member Analysis | Cosiety" />
<title>Member Analysis | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Analyse - Members</h1>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="member.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/group3.png" class="circle-icon" alt="Members"  title="Members">
            </div>
            <p class="black-text white-div-title">Members</p>
            <p class="white-div-number">200</p>
        </div>
    </a>
	<a href="memberPlan.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/plan.png" class="circle-icon" alt="Outstanding Plan"  title="Outstanding Plan">
            </div>
            <p class="black-text white-div-title">0 Plan Members</p>
            <p class="white-div-number">50</p>
        </div>
    </a>   
	<a href="paidPlan.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/duration.png" class="circle-icon" alt="Avg. Plan Duration"  title="Avg. Plan Duration">
            </div>
            <p class="black-text white-div-title">Avg. Plan Duration</p>
            <p class="white-div-number">2 months</p>
        </div>
    </a>   
	<a href="revenue.php">
        <div class="four-white-div four-two">
            <div class="color-circle color-circle4">
                <img src="img/revenue.png" class="circle-icon" alt="Total Revenue (RM)"  title="Total Revenue (RM)">
            </div>
            <p class="black-text white-div-title">Total Revenue (RM)</p>
            <p class="white-div-number">2000.00</p>
        </div>
    </a> 
    <div class="clear"></div>        


    <div class="half-four-div">
	<h1 class="backend-title-h1">Last 28 days</h1>       
	<div class="small-divider width100"></div>    
        <a href="member.php">
            <div class="four-white-div">
                <div class="color-circle color-circle5">
                    <img src="img/new-member.png" class="circle-icon" alt="New Members"  title="New Members">
                </div>
                <p class="black-text white-div-title">New Members</p>
                <p class="white-div-number">4</p>
            </div>
        </a>     
        <a href="revenue.php">
            <div class="four-white-div second-four-white-div">
                <div class="color-circle color-circle6">
                    <img src="img/revenue.png" class="circle-icon" alt="Revenue (RM)"  title="Revenue (RM)">
                </div>
                <p class="black-text white-div-title">Revenue (RM)</p>
                <p class="white-div-number">200.00</p>
            </div>
        </a> 
    </div>
    <div class="half-four-div second-half-four-div">
		<h1 class="backend-title-h1">Compare to last month</h1> 
        <div class="small-divider width100"></div>     
        <a href="member.php">
            <div class="four-white-div">
                <div class="color-circle color-circle7">
                    <img src="img/new-member.png" class="circle-icon" alt="New Members"  title="New Members">
                </div>
                <p class="black-text white-div-title">New Members</p>
                <p class="white-div-number">+ 1</p>
            </div>
        </a>
        <a href="revenue.php">
            <div class="four-white-div  second-four-white-div">
                <div class="color-circle color-circle4">
                    <img src="img/revenue.png" class="circle-icon" alt="Revenue (RM)"  title="Revenue (RM)">
                </div>
                <p class="black-text white-div-title">Total Revenue (RM)</p>
                <p class="white-div-number">+ 100.00</p>
            </div>
        </a>
     </div> 
     <div class="clear"></div>
     <div class="small-divider"></div>
     <div class="fillup-extra-space"></div><a href="member.php"><div class="blue-btn payment-button clean next-btn">Full Report</div></a>
     <div class="clear"></div>
     <div class="divider"></div>       
                     
</div>


<?php include 'js.php'; ?>
</body>
</html>