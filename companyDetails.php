<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Company Details | Cosiety" />
<title>Company Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Company Details</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Company Name</p>
        <p class="black-text answer-p">XXX Company</p>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Empoyer</p>
		<p class="black-text answer-p">Janice Lim</p>
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Employee 1</p>
        <p class="black-text answer-p">Alicia Tang</p>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Employee 2</p>
		<p class="black-text answer-p">Angela Tang</p>
	</div>    
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Employee 3</p>
        <p class="black-text answer-p">May Tang</p>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Employee 4</p>
		<p class="black-text answer-p">Jessie Tang</p>
	</div>    
	<div class="clear"></div>    
    <h2 class="backend-title-h2 review-title">Company Collaboration</h2>  
    <div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Company 1</p>
        <p class="black-text answer-p">ABC Company</p>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Company 2</p>
		<p class="black-text answer-p">EFG Company</p>
	</div>  
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="clear"></div> 
        <div class="small-divider"></div>
        <div class="width100 overflow">
            <p class="grey-text input-top-p">Reason of Blacklist</p>
            <input class="three-select clean" placeholder="Type the reason here" type="text">
		</div>
        <div class="small-divider"></div>
        <div class="clear"></div>
        <div class="width100 overflow receipt-two-btn-container">
        	<div class="fillup-2-btn-space"></div>
        	<div class="clean print-btn text-center"    onclick="goBack()">Back</div>
        	<button class="payment-button clean next-btn view-plan-btn red-btn">Blacklist</button>
        	<div class="fillup-2-btn-space"></div>
        </div>  
   
</div>


<?php include 'js.php'; ?>
</body>
</html>