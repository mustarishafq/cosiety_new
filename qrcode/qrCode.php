<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>QR Code</title>
  </head>
  <body>

  </body>
</html>
<?php include '../meta.php'; ?>
<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Booking.php';
require_once dirname(__FILE__) . '/../classes/BookingPrivate.php';
require_once dirname(__FILE__) . '/../classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $data = rewrite($_POST['data']);
}

$conn = connDB();

$roomDetailPrivate = getPrivate($conn, "WHERE booking_id = ? ", array("booking_id"), array($data), "s");
$roomDetailWorkDesk = getWorkDesk($conn, "WHERE booking_id = ? ", array("booking_id"), array($data), "s");
    //set it to writable location, a place for temp generated PNG files
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;

    $PNG_WEB_DIR = 'temp/';

    include "qrlib.php";

    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);


    $filename = $PNG_TEMP_DIR.'test.png';

    $errorCorrectionLevel = 'L';
    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
        $errorCorrectionLevel = $_REQUEST['level'];

    $matrixPointSize = 4;
    if (isset($_REQUEST['size']))
        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);




if ($roomDetailPrivate) {
  for ($cnt=0; $cnt < count($roomDetailPrivate) ; $cnt++) {

    if (isset($_REQUEST['data'])) {
        if (trim($_REQUEST['data']) == '')
            die('data cannot be empty! <a href="?">back</a>');
        $filename = $PNG_TEMP_DIR.'test'.md5($roomDetailPrivate[$cnt]->getBookingID().$roomDetailPrivate[$cnt]->getSeatID().'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($roomDetailPrivate[$cnt]->getBookingID().$roomDetailPrivate[$cnt]->getSeatID(), $filename, $errorCorrectionLevel, $matrixPointSize, 2);

    } else {
        echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';
        QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);
      }
    echo '<div style="width: 100%;text-align: center;margin-top: calc(50vh - 217.5px);">
			<p style="margin:0; margin-bottom:25px;"><img src="../img/cosiety-logo.png"></p>
      <h2 style="font-family:arial;margin-top: 0;margin-bottom: 15px;font-size: 24px;">Seat No : '.$roomDetailPrivate[$cnt]->getSeatID().'</h2>
			<h2 style="font-family:arial;margin-top: 0;margin-bottom: 15px;font-size: 24px;">Scan to Access</h2>
			<img src="'.$PNG_WEB_DIR.basename($filename).'"  style="width:250px;"/>
		  </div>';
  }
}
if ($roomDetailWorkDesk) {
  for ($cnt=0; $cnt < count($roomDetailWorkDesk) ; $cnt++) {

    if (isset($_REQUEST['data'])) {
        if (trim($_REQUEST['data']) == '')
            die('data cannot be empty! <a href="?">back</a>');
        $filename = $PNG_TEMP_DIR.'test'.md5($roomDetailWorkDesk[$cnt]->getBookingID().$roomDetailWorkDesk[$cnt]->getSeatID().'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
        QRcode::png($roomDetailWorkDesk[$cnt]->getBookingID().$roomDetailWorkDesk[$cnt]->getSeatID(), $filename, $errorCorrectionLevel, $matrixPointSize, 2);

    } else {
        echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';
        QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);
      }
    echo '<div style="width: 100%;text-align: center;margin-top: calc(50vh - 217.5px);">
			<p style="margin:0; margin-bottom:25px;"><img src="../img/cosiety-logo.png"></p>
      <h2 style="font-family:arial;margin-top: 0;margin-bottom: 15px;font-size: 24px;">Seat No : '.$roomDetailWorkDesk[$cnt]->getSeatID().'</h2>
			<h2 style="font-family:arial;margin-top: 0;margin-bottom: 15px;font-size: 24px;">Scan to Access</h2>
			<img src="'.$PNG_WEB_DIR.basename($filename).'"  style="width:250px;"/>
		  </div>';
  }
}
    //display generated file
