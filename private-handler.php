<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();



$roomDetails = getPrivate($conn);
// $endDate = $roomDetails[0]->getEndDate();
$currentDate = date("Y-m-d");
$active = 1;
$expired = 0;
$count = 1;

$start_date = null;
$end_date = null;
$duration = null;
$payment_amount = null;
$payment_verify = null;
$orderBy = null;

// $endDate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($date))."+ $time"));

if ($roomDetails) {
  for ($cnt=0; $cnt <count($roomDetails) ; $cnt++) {
    $totalCount = $count++;
    $endDate = $roomDetails[$cnt]->getEndDate();
    $endDate = date("Y-m-d",strtotime($endDate));

    if ($currentDate > $endDate) {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";

      if($active)
      {   $expired = 0;
          array_push($tableName,"seat_status");
          array_push($tableValue,$expired);
          $stringType .=  "i";
      }
      if($active)
      {
          array_push($tableName,"start_date");
          array_push($tableValue,$start_date);
          $stringType .=  "s";
      }
      if($active)
      {
          array_push($tableName,"end_date");
          array_push($tableValue,$end_date);
          $stringType .=  "s";
      }
      if($active)
      {
          array_push($tableName,"duration");
          array_push($tableValue,$duration);
          $stringType .=  "s";
      }if($active)
      {
          array_push($tableName,"payment_amount");
          array_push($tableValue,$payment_amount);
          $stringType .=  "s";
      }
      if($active)
      {
          array_push($tableName,"payment_verify");
          array_push($tableValue,$payment_verify);
          $stringType .=  "s";
      }
      if($active)
      {
          array_push($tableName,"orderBy");
          array_push($tableValue,$orderBy);
          $stringType .=  "s";
      }

      array_push($tableValue,$totalCount);
      $stringType .=  "i";
      $roomUpdate = updateDynamicData($conn,"booking_private"," WHERE seat_id = ? ",$tableName,$tableValue,$stringType);
      if($roomUpdate)
      {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../addBooking.php?type=1');
          echo "done";
      }
      else
      {
          echo "fail";

      }
    }else {
      $tableName = array();
      $tableValue =  array();
      $stringType =  "";

      if($active)
      {
          array_push($tableName,"seat_status");
          array_push($tableValue,$active);
          $stringType .=  "s";
      }
      array_push($tableValue,$totalCount);
      $stringType .=  "i";
      $roomUpdate = updateDynamicData($conn,"booking_private"," WHERE seat_id = ? ",$tableName,$tableValue,$stringType);
      if($roomUpdate)
      {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../addBooking.php?type=1');
          echo "done";
      }
      else
      {
          echo "fail";

      }
    }
  }
}









 ?>
