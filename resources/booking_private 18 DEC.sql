-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 09:11 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_private`
--

CREATE TABLE `booking_private` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `qr_code_id` varchar(255) NOT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_private`
--

INSERT INTO `booking_private` (`id`, `seat_id`, `booking_id`, `qr_code_id`, `seat_status`, `start_date`, `duration`, `end_date`, `total`, `payment_amount`, `payment_time`, `reference`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(88, '73', 'c4227cd03f4de0a51c8ef1b08e474adf', 'c4227cd03f4de0a51c8ef1b08e474adf73', 0, '2019-12-17 16:00:00.0', '1', '2020-01-18', NULL, '1280.00', NULL, NULL, 'APPROVED', 'testing', '2019-12-18 08:05:07', '2019-12-18 08:05:39'),
(89, '74', 'c4227cd03f4de0a51c8ef1b08e474adf', 'c4227cd03f4de0a51c8ef1b08e474adf74', 0, '2019-12-17 16:00:00.0', '1', '2020-01-18', NULL, '1280.00', NULL, NULL, 'APPROVED', 'testing', '2019-12-18 08:05:07', '2019-12-18 08:05:45'),
(90, '1', 'a94824075d7a0b8a2e4f77df4a653cb1', 'a94824075d7a0b8a2e4f77df4a653cb11', 0, '2019-12-17 16:00:00.0', '1', '2020-01-18', NULL, '1280.00', NULL, NULL, 'PENDING', 'testing', '2019-12-18 08:10:35', '2019-12-18 08:10:35'),
(91, '2', 'a94824075d7a0b8a2e4f77df4a653cb1', 'a94824075d7a0b8a2e4f77df4a653cb12', 0, '2019-12-17 16:00:00.0', '1', '2020-01-18', NULL, '1280.00', NULL, NULL, 'PENDING', 'testing', '2019-12-18 08:10:35', '2019-12-18 08:10:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_private`
--
ALTER TABLE `booking_private`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_private`
--
ALTER TABLE `booking_private`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
