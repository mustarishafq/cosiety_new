-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2019 at 09:44 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_private`
--

CREATE TABLE `booking_private` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_private`
--

INSERT INTO `booking_private` (`id`, `seat_id`, `booking_id`, `seat_status`, `start_date`, `duration`, `end_date`, `total`, `payment_amount`, `payment_time`, `reference`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(61, '1', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(62, '2', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(63, '3', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(64, '12', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(65, '13', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(66, '14', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06'),
(67, '23', 'fe3cd0e496d8ad3696ef6d799fc24392', 1, '2019-12-10 16:00:00.0', '1', '2020-01-11', NULL, '4480.00', NULL, NULL, 'PENDING', 'testing', '2019-12-11 08:41:06', '2019-12-11 08:41:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_private`
--
ALTER TABLE `booking_private`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_private`
--
ALTER TABLE `booking_private`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
