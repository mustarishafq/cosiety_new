-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2019 at 05:12 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety_old`
--

-- --------------------------------------------------------

--
-- Table structure for table `room_price`
--

CREATE TABLE `room_price` (
  `id` int(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `roomcapacity` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display` smallint(1) NOT NULL DEFAULT 1,
  `price` decimal(50,2) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `discount` int(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_price`
--

INSERT INTO `room_price` (`id`, `type_id`, `type`, `roomcapacity`, `name`, `display`, `price`, `duration`, `discount`, `description`) VALUES
(1, 1, 'Lounge', 1, 'Yearly Membership', 1, '1999.00', 'Month', 20, 'RM999 only if sign up on Nov 2019'),
(2, 1, 'Lounge', 1, 'Monthly Membership', 1, '199.00', 'Month', 20, 'RM99 only if sign up on Nov 2019\r\n'),
(3, 1, 'Lounge', 1, 'Daily Pass', 1, '30.00', 'Day', 20, 'RM20 only if sign up on Nov 2019\r\n'),
(4, 2, 'Work Desk', 1, 'Dedicated Work Desk', 1, '799.00', 'Month', 20, 'Enjoy 20% off from the monthly rental\r\n\r\n'),
(5, 2, 'Work Desk', 1, 'Co-Working Space (Hot Seat)', 1, '399.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(6, 3, 'Private Suit', 1, '1 Work Station', 1, '1000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(7, 3, 'Private Suit', 2, '2 Work Station', 1, '1600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(8, 3, 'Private Suit', 3, '3 Work Stations', 1, '2400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(9, 3, 'Private Suit', 4, '4 Work Stations', 1, '3200.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(10, 3, 'Private Suit', 5, '5 Work Stations', 1, '4000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(11, 3, 'Private Suit', 6, '6 Work Stations', 1, '4800.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(12, 3, 'Private Suit', 7, '7 Work Stations', 1, '5600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(13, 3, 'Private Suit', 8, '8 Work Stations', 1, '6400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(14, 4, 'Meeting Room', 0, 'Meeting Room', 1, '0.00', 'Hour', 20, 'Comfortable environment');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `room_price`
--
ALTER TABLE `room_price`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `room_price`
--
ALTER TABLE `room_price`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419140;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
