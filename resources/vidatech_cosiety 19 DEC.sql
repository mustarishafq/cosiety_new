-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 07:43 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cosiety`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `cost` decimal(50,2) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `timeline` text DEFAULT NULL,
  `start_date` timestamp(3) NULL DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total_ppl` varchar(255) DEFAULT NULL,
  `order_by` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `total_price` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` timestamp(1) NULL DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `total_seat` varchar(255) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_details` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `receipt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `uid`, `booking_id`, `area_type`, `cost`, `duration`, `timeline`, `start_date`, `end_date`, `total_ppl`, `order_by`, `discount`, `total_price`, `payment_amount`, `payment_time`, `payment_verify`, `total_seat`, `project_title`, `project_details`, `payment_method`, `date_created`, `date_updated`, `receipt`) VALUES
(86, 'eb3b449f0c4868acf7cebaa02887a3e6', '01e0500661be82d8fd36f98f7364243d', 'Dedicated Work Desk', '1598.00', '1', 'Month', '2019-12-18 16:00:00.000', '2020-01-19', '2', 'testing', '20', '1278.40', '1278.40', '2019-12-19 06:30:00.0', 'APPROVED', '2', NULL, NULL, 'Online Banking', '2019-12-19 06:30:50', '2019-12-19 06:35:03', 'SeverusSnape.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `booking_lounge`
--

CREATE TABLE `booking_lounge` (
  `id` bigint(20) NOT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_meeting`
--

CREATE TABLE `booking_meeting` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_private`
--

CREATE TABLE `booking_private` (
  `id` bigint(20) NOT NULL,
  `seat_id` varchar(255) DEFAULT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `qr_code_id` varchar(255) NOT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total` decimal(50,2) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_workdesk`
--

CREATE TABLE `booking_workdesk` (
  `id` int(11) NOT NULL,
  `seat_id` varchar(255) NOT NULL,
  `booking_id` varchar(255) DEFAULT NULL,
  `qr_code_id` varchar(255) DEFAULT NULL,
  `seat_status` int(2) NOT NULL DEFAULT 0 COMMENT '0=available, 1=booked',
  `start_date` timestamp(1) NULL DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(50,2) DEFAULT NULL,
  `payment_time` timestamp(1) NULL DEFAULT NULL,
  `payment_verify` varchar(255) DEFAULT NULL,
  `orderBy` varchar(255) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_workdesk`
--

INSERT INTO `booking_workdesk` (`id`, `seat_id`, `booking_id`, `qr_code_id`, `seat_status`, `start_date`, `duration`, `end_date`, `payment_amount`, `payment_time`, `payment_verify`, `orderBy`, `dateCreated`, `dateUpdated`) VALUES
(37, '1', '01e0500661be82d8fd36f98f7364243d', '01e0500661be82d8fd36f98f7364243d1', 0, '2019-12-18 16:00:00.0', '1', '2020-01-19', '1278.40', '2019-12-19 06:30:00.0', 'PENDING', 'testing', '2019-12-19 06:30:50', '2019-12-19 06:30:50'),
(38, '2', '01e0500661be82d8fd36f98f7364243d', '01e0500661be82d8fd36f98f7364243d2', 0, '2019-12-18 16:00:00.0', '1', '2020-01-19', '1278.40', '2019-12-19 06:30:00.0', 'PENDING', 'testing', '2019-12-19 06:30:50', '2019-12-19 06:30:50');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT 'NULL',
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `receipt`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_method`, `payment_amount`, `payment_bankreference`, `payment_date`, `payment_time`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `date_created`, `date_updated`) VALUES
(66, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-16 04:57:39', '2019-12-16 04:57:39'),
(67, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-16 04:59:12', '2019-12-16 04:59:12'),
(68, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-16 09:57:04', '2019-12-16 09:57:04'),
(69, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, 'backup.PNG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:28:46', '2019-12-19 04:28:46'),
(70, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, 'backup.PNG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:28:46', '2019-12-19 04:28:46'),
(71, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:30:52', '2019-12-19 04:30:52'),
(72, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:34:01', '2019-12-19 04:34:01'),
(73, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:34:44', '2019-12-19 04:34:44'),
(74, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:43:21', '2019-12-19 04:43:21'),
(75, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 04:45:12', '2019-12-19 04:45:12'),
(76, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 06:17:06', '2019-12-19 06:17:06'),
(77, 'eb3b449f0c4868acf7cebaa02887a3e6', 'testing', NULL, NULL, NULL, 'SeverusSnape.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-19 06:30:50', '2019-12-19 06:30:50');

-- --------------------------------------------------------

--
-- Table structure for table `room_price`
--

CREATE TABLE `room_price` (
  `id` int(255) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `roomcapacity` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `display` smallint(1) NOT NULL DEFAULT 1,
  `price` decimal(50,2) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `discount` int(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_price`
--

INSERT INTO `room_price` (`id`, `type_id`, `type`, `roomcapacity`, `name`, `display`, `price`, `duration`, `discount`, `description`) VALUES
(1, 1, 'Lounge', 1, 'Yearly Membership', 1, '1999.00', 'Month', 20, 'RM999 only if sign up on Nov 2019'),
(2, 1, 'Lounge', 1, 'Monthly Membership', 1, '199.00', 'Month', 20, 'RM99 only if sign up on Nov 2019\r\n'),
(3, 1, 'Lounge', 1, 'Daily Pass', 1, '30.00', 'Day', 20, 'RM20 only if sign up on Nov 2019\r\n'),
(4, 2, 'Work Desk', 1, 'Dedicated Work Desk', 1, '799.00', 'Month', 20, 'Enjoy 20% off from the monthly rental\r\n\r\n'),
(5, 2, 'Work Desk', 1, 'Co-Working Space (Hot Seat)', 1, '399.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(6, 3, 'Private Suit', 1, '1 Work Station', 1, '1000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(7, 3, 'Private Suit', 2, '2 Work Station', 1, '1600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(8, 3, 'Private Suit', 3, '3 Work Stations', 1, '2400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(9, 3, 'Private Suit', 4, '4 Work Stations', 1, '3200.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(10, 3, 'Private Suit', 5, '5 Work Stations', 1, '4000.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(11, 3, 'Private Suit', 6, '6 Work Stations', 1, '4800.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(12, 3, 'Private Suit', 7, '7 Work Stations', 1, '5600.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(13, 3, 'Private Suit', 8, '8 Work Stations', 1, '6400.00', 'Month', 20, 'Enjoy 20% off from the monthly rental'),
(14, 4, 'Meeting Room', 1, 'Meeting Room', 1, '0.00', 'Hour', 20, 'Comfortable environment');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `full_name`, `country`, `login_type`, `user_type`, `date_created`, `date_updated`, `birthday`, `gender`) VALUES
('007d16ccdbb86aa21b8e7c9d72183e4c', 'user', 'user@gmail.com', '6e07d6d251f7026c23539a6ce3ab2c68fc58b9eee282c2ae5657489873f37a03', '2d067165e4209e92b239ed9f4a909b7ad65a3b35', '', NULL, NULL, 'Malaysia', 1, 1, '2019-12-13 08:18:25', '2019-12-13 08:18:25', NULL, NULL),
('0aaed93516bb13d05c6eea18d4a9efb0', 'AAA', 'AAA@gmail.com', '170d8649e2c1347581f718bef08f29ce7c2a622f66a588032e7cd2bcd5c3f54e', 'c464f6baf06e29a40d30a489cebe6295b6b6599a', '', NULL, NULL, 'Malaysia', 1, 1, '2019-12-13 07:39:29', '2019-12-13 07:39:29', NULL, NULL),
('68a18ec9b977f0aa47a901fccdd666d4', 'aaaaaa', 'aa@gg.cc', 'd29a1fbf3d0e24f0a71c5f849d78cf66f71f4af086efda7ba968718072eeca53', 'ea9158d9a55b9404ef39423715c3245020fd1d9b', '111111', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 03:45:33', '2019-11-01 03:45:33', NULL, NULL),
('afac5eac642581a6ae8523f7623de49a', 'lolo', 'lolo@gg.nm', 'd1e7fa5b84cb55554724f777b9af5ec32fd4ed7853c25ebfc85702625f696281', '87ef210d9e902a1f9f90bb1827df4af5302799e0', '14141414', NULL, NULL, 'Malaysia', 1, 1, '2019-11-01 05:01:54', '2019-11-01 05:01:54', NULL, NULL),
('c875c4116e8343e728db0a439b2097ee', 'admin', 'admin@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '1233214455', NULL, NULL, 'Malaysia', 1, 0, '2019-11-01 03:26:22', '2019-11-28 03:24:28', NULL, NULL),
('eb3b449f0c4868acf7cebaa02887a3e6', 'testing', 'test@gg.cc', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', '111222555', NULL, 'Mustari Shafiq', 'Malaysia', 1, 1, '2019-10-31 03:51:51', '2019-12-11 07:33:54', '2019-12-11', 'Male'),
('f493b6c22a7bf8e80d1a725c747e994c', 'Guest', 'mike@gmail.com', '0503e3c64d65a5cabc8965c4cb7b17db42e2b53bb172938c8269a55950229473', 'f62a5d0f5a5a55a890a5eb2d587cbc8483d26cc6', '', NULL, NULL, 'Malaysia', 1, 1, '2019-12-12 07:14:53', '2019-12-12 07:14:53', NULL, NULL),
('f7c8cf4afec0dce4ccf677ac81661dec', 'zzzzz', 'zzz@gg.zz', '4f150972c7cef5240558a81dd6e136c13620defb6782a2fa05eeb71db0492ae6', 'a21926404c1d56d28829b210f76777c988b9f38b', '22222222', NULL, NULL, 'Singapore', 1, 1, '2019-10-31 09:33:15', '2019-10-31 09:33:15', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingUid_to_userUsername` (`uid`);

--
-- Indexes for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_meeting`
--
ALTER TABLE `booking_meeting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_private`
--
ALTER TABLE `booking_private`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_price`
--
ALTER TABLE `room_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `booking_lounge`
--
ALTER TABLE `booking_lounge`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `booking_meeting`
--
ALTER TABLE `booking_meeting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_private`
--
ALTER TABLE `booking_private`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `booking_workdesk`
--
ALTER TABLE `booking_workdesk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `room_price`
--
ALTER TABLE `room_price`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419140;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `bookingUid_to_userUsername` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
