<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Empty Space/Room Report | Cosiety" />
<title>Empty Space/Room Report | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<h1 class="backend-title-h1">Report | <a href="emptyRoom.php" class="lightblue-text hover-effect">Today</a></h1>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
        <div class="three-search-div">
			<div class="three-search-div second-three-search"><button class="three-search blue-btn clean search-blue-btn">Search</button></div>   
        </div>
    </div>
    <div class="clear"></div>
    <div class="small-divider"></div>
    <div class="width100 overflow">
    	<html ng-app="myApp" ng-controller="AppCtrl" lang="en">
        	<head>
            	<meta charset="utf-8">
                <title>Circle</title>
        	</head>
        	<body>
            	<div calendar class="calendar" id="calendar"></div>
         	</body>
    	</html>       
	</div>
    <div class="clear"></div>
    <div class="divider"></div>    
    <div class="width100">
    	<select class="clean title-select">
        	<option>September</option>
            <option>August</option>
            <option>July</option>
        </select>
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Space/Room</th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>  
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                        <th>21</th>
                        <th>22</th>
                        <th>23</th>
                        <th>24</th>
                        <th>25</th>
                        <th>26</th>
                        <th>27</th>  
                        <th>28</th>
                        <th>29</th>
                        <th>30</th>                         
                        <th>Total</th>                                                                         
                    </thead>
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>Private Suit 1 Work Station</td>
                    <td class="red-text">E</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>      
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>     
                    <td>-</td>
                    <td>-</td> 
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>      
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>     
                    <td>-</td>
                    <td>-</td>                     
                    <td>1</td>                                                   
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>Private Suit 2 Work Stations</td>
                    <td class="red-text">E</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>      
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>     
                    <td>-</td>
                    <td>-</td> 
                    <td>-</td> 
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>      
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>     
                    <td>-</td>
                    <td>-</td>                     
                    <td>1</td>  
                </tr>                
            </table>
		</div>
    </div>
  		<!--
    
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>