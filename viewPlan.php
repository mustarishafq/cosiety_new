<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="View Plan Details | Cosiety" />
<title>View Plan Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">View Plan Details</h1>

	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
	<textarea class="clean width100 project-textarea" placeholder="Key in Project Details"></textarea>  
  
    <h1 class="backend-title-h1">Register Visitor</h1>
	<h3>Visitor 1</h3>	
    <div class="three-div">
    	<p class="grey-text input-top-p">Visitor Name</p>
        <input class="three-select clean" placeholder="Name" type="text">
    </div>
	<div class="three-div middle-three-div second-three-div">
    	<p class="grey-text input-top-p">Start Time</p>
        <input class="three-select clean"  type="datetime-local">
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">End Time</p>
		<input class="three-select clean"  type="datetime-local">
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Contact No.</p>
		<input class="three-select clean" type="number">
    </div>
	<div class="three-div middle-three-div">
    	<p class="grey-text input-top-p">Meeting Room Access</p>
        <select class="three-select clean">
        	<option>Allow</option>
            <option>Not Allowed</option>
        </select>
    </div>    
    
    <div class="clear"></div>        
    <div class="hover-effect black-btn-add">Add</div>
    <div class="divider"></div>
    <div class="divider"></div>
    <div class="clear"></div> 
            <!--<div class="width100 overflow">
            	<div class="fillup-2-btn-space"></div>
                
                <a href="changeLocation.php"><div class="clean print-btn text-center">Change Location</div></a>
            	<button class="blue-btn payment-button clean next-btn view-plan-btn">Save</button>
                <div class="fillup-2-btn-space"></div>
            </div>   --> 
	<div class="fillup-extra-space"></div><a href="profile.php"><button class="blue-btn payment-button clean next-btn">Save</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space3"></div><a href="receipt.php" class="cancel-a hover-effect">View Receipt</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>