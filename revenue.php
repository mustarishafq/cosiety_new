<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Revenue | Cosiety" />
<title>Revenue | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Revenue: RM2,000.00</h1>
	<select class="clean align-h1-select issue-select">
    	<option>Latest</option>
        <option>Oldest</option>
        <option>Solved</option>
        <option>Unsolved</option>
    </select>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Company</p>
            <input class="search-input" type="text" placeholder="Company Name">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Employer</p>
            <input class="search-input" type="text" placeholder="Employer Name">
        </div>
        <div class="three-search-div">
        	<p class="upper-search-p">Plan</p>
            <select class="search-input">
            	<option>Lounge</option>
                <option>Dedicated Work Desk</option>
                <option>Co-Working Space (Hot Seat)</option>
                <option>Private Suit</option>                
            </select>
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div middle-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
        <div class="three-search-div second-three-search"><button class="three-search blue-btn clean search-blue-btn">Search</button></div>                
    </div>
    <div class="clear"></div>
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Company</th>
                        <th>Employer</th>
                        <th>Plan</th>
                        <th>Slot</th>
                        <th>Duration</th>
                        <th>Amount (RM)</th>
                        <th>Paid On</th>                        
                    </thead>
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>XXX Company</td>
                    <td>Janice Lim</td>
                    <td>Basic Plan C</td>
                    <td>8</td>
                    <td>1 Month</td>
                    <td>800.00</td>
                    <td>12/8/2019</td>                    
                </tr>
                <tr data-url="receipt.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>ABC Company</td>
                    <td>Jack Lim</td>
                    <td>Basic Plan C</td>
                    <td>6</td>
                    <td>2 Months</td>
                    <td>1200.00</td>
                    <td>12/6/2019</td>  
                </tr>                
            </table>
		</div>
    </div>
  		<!--
    
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>