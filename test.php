<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$privateDetails = getPrivate($conn, "WHERE seat_id = ? ", array("seat_id"), array(1), "i");

$startDate = date("d-m-Y", strtotime($privateDetails[0]->getStartDate()));
$endDate = date("d-m-Y", strtotime($privateDetails[0]->getEndDate()));

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div id="divtoshow" style="position: fixed;display:none;"><?php echo $startDate. " - " .$endDate ?></div>
<br><br>
<span onmouseover="hoverdiv(event,'divtoshow')" onmouseout="hoverdiv(event,'divtoshow')"> <input type="checkbox" name="" value="">1 </span>

<script type="text/javascript">
  function hoverdiv(e,divid){

  var left  = e.clientX  + "px";
  var top  = e.clientY  + "px";

  var div = document.getElementById(divid);

  div.style.left = left;
  div.style.top = top;

  $("#"+divid).toggle();
  return false;
}
</script>
