<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Profile | Cosiety" />
<title>Edit Profile | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<form method="POST" action="utilities/editProfileFunction.php">
		<h1 class="backend-title-h1">Edit Profile</h1>
		<div class="edit-half-div">
			<p class="grey-text input-top-p">Fullname</p>
			<input class="three-select clean" placeholder="Fullname" type="text" id="update_fullname" name="update_fullname" value="<?php echo $userDetails->getFullName();?>" required>
		</div>

		<input type="hidden" id="username" name="username" value="<?php echo $userDetails->getUsername();?>">

		<div class="edit-half-div second-edit-half-div">
			<p class="grey-text input-top-p">Position (unavble to update currently)</p>
			<select class="three-select clean">
				<option>Employer</option>
				<option>Employee</option>
			</select>
		</div>            
		<div class="clear"></div>
		<div class="edit-half-div">
			<p class="grey-text input-top-p">Company (<a href="createCompany.php" class="lightblue-text hover-effect">Create a New Company</a>/<a href="createCompany.php" class="orange-status hover-effect">Edit</a>)</p>
			<select class="three-select clean">
				<option>Company 1</option>
				<option>Company 2</option>
			</select>
		</div>
		<div class="edit-half-div second-edit-half-div">
			<p class="grey-text input-top-p">Email</p>
			<input class="three-select clean" type="email" value="<?php echo $userDetails->getEmail();?>" readonly>
			<!-- <input class="three-select clean" placeholder="janice@gmail.com" type="email"> -->
		</div>            
		<div class="clear"></div>
		<div class="edit-half-div">
			<p class="grey-text input-top-p">Country</p>
			
			<!-- <select class="three-select clean">
				<option>Malaysia</option>
				<option>Singapore</option>
			</select> -->
			
			<select class="three-select clean" id="update_country" name="update_country" value="<?php echo $userDetails->getCountry();?>">

			<?php
				if($userDetails->getCountry() == '')
				{
					?>
					<option value="Malaysia"  name='Malaysia'>Malaysia</option>
					<option value="Singapore"  name='Singapore'>Singapore</option>
					<option selected value=""  name=''></option>
					<?php
				}
				else if($userDetails->getCountry() == 'Malaysia')
				{
					?>
					<option value="Singapore"  name='Singapore'>Singapore</option>
					<option selected value="Malaysia"  name='Malaysia'>Malaysia</option>
					<?php
				}
				else if($userDetails->getCountry() == 'Singapore')
				{
					?>
					<option value="Malaysia"  name='Malaysia'>Malaysia</option>
					<option selected value="Singapore"  name='Singapore'>Singapore</option>
					<?php
				}
			?>

			</select>

		</div>

		<div class="edit-half-div second-edit-half-div">
			<p class="grey-text input-top-p">Contact  (example : +60127413695)</p>
			<input class="three-select clean" placeholder="contact" type="text" id="update_contact" name="update_contact" value="<?php echo $userDetails->getPhoneNo();?>" required>
			<!-- <span class="country-code2">+60</span><input class="three-select clean phone-input2" placeholder="14 533 000" type="number"> -->
		</div>     
		<div class="clear"></div>   
			<div class="edit-half-div">
			<p class="grey-text input-top-p">Gender</p>
					
			<select class="three-select clean" id="update_gender" name="update_gender" value="<?php echo $userDetails->getGender();?>">

			<?php
				if($userDetails->getGender() == '')
				{
					?>
					<option value="Female"  name='Female'>Female</option>
					<option value="Male"  name='male'>Male</option>
					<option selected value=""  name=''></option>
					<?php
				}
				else if($userDetails->getGender() == 'Male')
				{
					?>
					<option value="Female"  name='Female'>Female</option>
					<option selected value="Male"  name='male'>Male</option>
					<?php
				}
				else if($userDetails->getGender() == 'Female')
				{
					?>
					<option selected value="Female"  name='Female'>Female</option>
					<option value="Male"  name='male'>Male</option>
					<?php
				}
			?>

			</select>

		</div>
		<div class="edit-half-div second-edit-half-div">
			<p class="grey-text input-top-p">Birthday</p>
			<!-- <input class="three-select clean" type="date"> -->
			<input class="three-select clean" placeholder="contact" type="date" id="update_birthday" name="update_birthday" value="<?php echo $userDetails->getBirthday();?>">
		</div>    
		<div class="clear"></div>

		<!-- <div class="width100 overflow">
			<p class="grey-text input-top-p">About</p>
			<textarea class="clean width100 project-textarea edit-margin-btm" placeholder="About you"></textarea> 
		</div>     -->

		<div class="clear"></div> 

		<div class="fillup-extra-space"></div><a href="profile.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
	</form>

		<div class="clear"></div>
		<div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>