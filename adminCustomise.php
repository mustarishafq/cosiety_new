<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Customise | Cosiety" />
<title>Customise | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Customise</h1>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="promotion.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/promotion.png" class="circle-icon" alt="Current Promotion"  title="Current Promotion">
            </div>
            <p class="black-text white-div-title">Current Promotion</p>
            <p class="white-div-number">1</p>
        </div>
    </a>
	<a href="typeofSlot.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/seat3.png" class="circle-icon" alt="Type of Slot"  title="Type of Slot">
            </div>
            <p class="black-text white-div-title">Type of Slot</p>
            <p class="white-div-number">4</p>
        </div>
    </a>   
	<a href="customisePlan.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/plan-bill.png" class="circle-icon" alt="Plan"  title="Plan">
            </div>
            <p class="black-text white-div-title">Plan</p>
            <p class="white-div-number">4</p>
        </div>
    </a>   
	<a href="rules.php">
        <div class="four-white-div four-two">
            <div class="color-circle color-circle4">
                <img src="img/booking2.png" class="circle-icon" alt="Rules and Regulations"  title="Rules and Regulations">
            </div>
            <p class="black-text white-div-title">Rules and Regulations</p>
            <p class="white-div-number">&nbsp;</p>
        </div>
    </a>  
	<a href="faq.php">
        <div class="four-white-div third-two">
            <div class="color-circle color-circle5">
                <img src="img/booking2.png" class="circle-icon" alt="FAQ"  title="FAQ">
            </div>
            <p class="black-text white-div-title">FAQ</p>
            <p class="white-div-number">2</p>
        </div>
    </a>                
</div>


<?php include 'js.php'; ?>
</body>
</html>