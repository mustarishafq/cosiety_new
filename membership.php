<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $adminList = getBooking($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$normalBookingDetails = getBooking($conn);
$bookingDetails = getBooking($conn, "WHERE payment_verify = ?", array("payment_verify"), array("APPROVED"), "s");
$bookingPendingDetails = getBooking($conn, "WHERE payment_verify = ?", array("payment_verify"), array("PENDING"), "s");

$currentDate = date("Y-m-d");

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Membership | Cosiety" />
<title>Membership | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">My Ongoing Plan</h1>
    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/calendar.png" class="header-icon" alt="My Ongoing Plan" title="My Ongoing Plan"> <p>Ongoing Plan</p>
                <!--<a href="" class="hover-effect white-text view-a">View All</a>-->
            </div>
						<?php if ($bookingDetails) {
							for ($cnt=0; $cnt < count($bookingDetails) ; $cnt++) {
								if ($currentDate >= date("Y-m-d", strtotime($bookingDetails[$cnt]->getStartDate())) && $currentDate <= date("Y-m-d", strtotime($bookingDetails[$cnt]->getEndDate())) ) {

									?>  <div class="white-box-content">
				            	<a href="receipt.php" class="hover-effect">
				                    <div class="content-container">
				                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
				                        <div class="right-icon-div">
				                            <p class="light-grey-text small-date hover-effect">    <?php echo date("d-m-Y", strtotime($bookingDetails[$cnt]->getStartDate())) ?> ➠ <?php echo date("d-m-Y", strtotime($bookingDetails[$cnt]->getEndDate())) ?></p>
				                            <p class="white-box-content-p hover-effect"><?php echo $bookingDetails[$cnt]->getAreaType() ?></p>
				                        </div>
				                    </div>
				                </a>
				            </div><?php

								}else {

								}

							}
						}else {
						} ?>


        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/bill.png" class="header-icon" alt="Upcoming Payment" title="Upcoming Payment"> <p>Pending Verification</p>
                <!--<a href="booking.php" class="hover-effect white-text view-a">View All</a>-->
            </div>
						<?php if ($bookingPendingDetails) {
							for ($cnt=0; $cnt < count($bookingPendingDetails) ; $cnt++) {
								?><div class="white-box-content">
		            	<a href="paymentMethod.php" class="hover-effect">
		                    <div class="content-container">
		                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Lounge" title="Lounge"></div>
		                        <div class="right-icon-div">
		                            <p class="light-grey-text small-date hover-effect left-date"><?php echo date("d-m-Y", strtotime($bookingPendingDetails[$cnt]->getStartDate())) ?> ➠ <?php echo date("d-m-Y", strtotime($bookingPendingDetails[$cnt]->getEndDate())) ?> <span class="green-status">(PENDING)</span></p><p class="black-text right-price">RM <?php echo $bookingPendingDetails[$cnt]->getPaymentAmount() ?></p>
		                            <p class="white-box-content-p hover-effect clear"><?php echo $bookingPendingDetails[$cnt]->getAreaType() ?></p>
		                        </div>
		                    </div>
		                </a>
		            </div><?php
							}

						} ?>

        </div>
    </div>

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
            <div class="color-header blue-header top-radius">
                <img src="img/expired-plan.png" class="header-icon" alt="Expired Plan" title="Expired Plan"> <p>Expired Plan</p>
                <a href="allPlan.php" class="hover-effect white-text view-a">View All</a>
            </div>
						<?php if ($normalBookingDetails) {
							for ($cnt=0; $cnt < count($normalBookingDetails) ; $cnt++) {
								$endDate = date("Y-m-d", strtotime($normalBookingDetails[$cnt]->getEndDate()));
								if ($currentDate > $endDate) {
									?><div class="white-box-content">
			            	<a href="receipt.php" class="hover-effect">
			                    <div class="content-container">
			                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
			                        <div class="right-icon-div">
			                            <p class="light-grey-text small-date hover-effect left-date">Expire on <?php echo $normalBookingDetails[$cnt]->getEndDate(); ?> <?php if ($normalBookingDetails[$cnt]->getPaymentVerify() == "PENDING") {
			                            	?><span class="green-status">(PENDING)</span><?php
			                            }else {
			                            	?><span class="green-status">(PAID)</span><?php
			                            } ?></p><p class="black-text right-price">RM99.00</p>
			                            <p class="white-box-content-p hover-effect clear"><?php echo $normalBookingDetails[$cnt]->getAreaType() ?></p>
			                        </div>
			                    </div>
			                </a>

			            </div><?php
								}else {
									?><div class="white-box-content">


			            </div><?php
								}
							}
						} ?>

        </div>
        <div class="two-box-div overflow second-box image-container">
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingPrivate.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>


</div>


<?php include 'js.php'; ?>
</body>
</html>
