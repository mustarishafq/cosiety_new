<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Calendar Settings | Cosiety" />
<title>Calendar Settings | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Calendar Settings</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Date</p>
        <input class="three-select clean" type="date">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Status</p>
        <select class="three-select clean">
        	<option>Open</option>
            <option>Closed/Unavailable</option>
        </select>
	</div>            
	<div class="clear"></div>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">Remark</p>
		<textarea class="clean width100 project-textarea edit-margin-btm" placeholder="Remark"></textarea>     	
    </div>           
	<div class="divider"></div>
    <div class="clear"></div>
	<div class="width100 overflow">
	<div class="fillup-2-btn-space"></div>
	<button class="clean print-btn"  onclick="goBack()">Cancel</button>
	<a href="viewCalendar.php"><button class="blue-btn payment-button clean next-btn view-plan-btn">Confirm</button></a>
	<div class="fillup-2-btn-space"></div>
	</div>
	<div class="clear"></div>
</div>


<?php include 'js.php'; ?>
</body>
</html>