<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $area_type = rewrite($_POST["area_type"]);
    $cost = rewrite($_POST["cost"]);
}


$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details | Cosiety" />
<title>Booking Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
<form action="paymentMethod.php" method="POST">
<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->
<!-- <h4> <?php //echo $area_type?> </h4> -->
<!-- <h4> <?php //echo $cost?> </h4> -->

    <!-- <h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1> -->
    <h1 class="backend-title-h1"><?php echo $area_type?></h1>

    <input type="hidden" name="area_type" id="area_type" value="<?php echo $area_type?>">
    <input type="hidden" name="cost" id="cost" value="<?php echo $cost?>">

	<div class="three-div">
    	<p class="grey-text input-top-p">Duration</p>
        <select class="three-select clean" id="duration" name="duration">
            <option value="1 month" name="1 month">1 month</option>
            <option value="2 month" name="2 month">2 month</option>
            <option value="3 month" name="3 month">3 month</option>
        </select>
    </div>
	<div class="three-div middle-three-div second-three-div">
        <p class="grey-text input-top-p">Start Date</p>
        <input class="three-select clean" type="date" id="start_date" name="start_date">
        <!-- <input type="date" class="three-select clean"> -->
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">How Many Person?</p>
        <select class="three-select clean" id="total_ppl" name="total_ppl">
            <option value="3" name="3">3</option>
            <option value="4" name="4">4</option>
            <option value="5" name="5">5</option>
            <option value="6" name="6">6</option>
            <option value="7" name="7">7</option>
            <option value="8" name="8">8</option>
            <option value="9" name="9">9</option>
            <option value="10" name="10">10</option>                                                     
        </select>
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">By</p>
        <select class="three-select clean" id="order_by" name="order_by">
        	<option value="XXX Company" name="XXX Company">XXX Company</option>
            <option value="Personal" name="Personal">Personal</option>
        </select>
    </div>
	<div class="three-div middle-three-div">
    	<p class="grey-text input-top-p">Discount</p>
        <p class="three-select-p">20%</p>
        <input type="hidden" name="discount" id="discount" value="20%">
    </div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Total</p>
        <p class="total-p">RM639.20</p>
        <input type="hidden" name="total_price" id="total_price" value="RM639.20">
    </div> 
    <div class="clear"></div>
	<h2 class="backend-title-h2">Floor Plan</h2>    
    <img src="img/floor-plan.jpg" alt="Floor Plan" title="Floor Plan" class="width100">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space1.jpg"  data-fancybox="images-preview" title="Co-Working Space (Hot Seat)">
                <img src="img/working-space1.jpg" class="width100 opacity-hover" alt="Co-Working Space (Hot Seat)" title="Co-Working Space (Hot Seat)">
            </a>  
            <p class="four-img-p">Co-Working Space (Hot Seat)</p>  	
        </div>
        <div class="four-img-div middle-four-img-div1">
            <a href="./img/working-space2.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space2.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>  
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space3.jpg"  data-fancybox="images-preview" title="Lounge">
                <img src="img/working-space3.jpg" class="width100 opacity-hover" alt="Lounge" title="Lounge">
            </a>  
            <p class="four-img-p">Lounge</p>  	
        </div> 
        <div class="four-img-div">
            <a href="./img/working-space4.jpg"  data-fancybox="images-preview" title="Private Suit 1">
                <img src="img/working-space4.jpg" class="width100 opacity-hover" alt="Private Suit 1" title="Private Suit 1">
            </a>  
            <p class="four-img-p">Private Suit 1</p>  	
        </div>                           
	</div>
    <!-- <h2 class="backend-title-h2">Choose your seat (3)</h2> -->
    <h2 class="backend-title-h2">Choose your seat</h2>
        <select class="three-select clean" id="total_seat" name="total_seat">
            <option value="1" name="1">1</option>
            <option value="2" name="2">2</option>
            <option value="3" name="3">3</option>
            <option value="4" name="4">4</option>
            <option value="5" name="5">5</option>
            <option value="6" name="6">6</option>
            <option value="7" name="7">7</option>
            <option value="8" name="8">8</option>
            <option value="9" name="9">9</option>
            <option value="10" name="10">10</option>                                                     
        </select>    
    <!-- <div class="big-container-for-seat">
    	<div class="eight-checkbox">
            <label class="container1"> 1
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 2
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 3
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 4
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 5
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 6
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 7
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 8
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 9
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 10
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 11
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 12
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 13
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 14
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 15
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 16
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 17
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 18
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>                                                                                 
    </div> -->
    <div class="clear"></div>

	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title" id="project_title" name="project_title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
    <textarea class="clean width100 project-textarea" placeholder="Key in Project Details" id="project_details" name="project_details"></textarea>  
       
    <!-- <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a> -->

    <div class="fillup-extra-space"></div>
    <!-- <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Proceed to Payment</button> -->
    <button input type="submit" name="submit" value="Submit" class="blue-btn payment-button clean">Submit</button>

    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</form>
</div>


<?php include 'js.php'; ?>
</body>
</html>