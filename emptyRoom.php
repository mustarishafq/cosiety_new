<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Empty Space/Room | Cosiety" />
<title>Empty Space/Room | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Today | <a href="emptyRoomReport.php" class="lightblue-text hover-effect">Report</a></h1>

    <div class="clear"></div>
	<h2 class="backend-title-h2">Floor Plan</h2>    
    <img src="img/floor-plan.jpg" alt="Floor Plan" title="Floor Plan" class="width100">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space1.jpg"  data-fancybox="images-preview" title="Working Space 1-5">
                <img src="img/working-space1.jpg" class="width100 opacity-hover" alt="Working Space 1-5" title="Working Space 1-5">
            </a>  
            <p class="four-img-p">Co-Working Space (Hot Seat)</p>  	
        </div>
        <div class="four-img-div middle-four-img-div1">
            <a href="./img/working-space2.jpg"  data-fancybox="images-preview" title="Working Space 7-11">
                <img src="img/working-space2.jpg" class="width100 opacity-hover" alt="Working Space 7-11" title="Working Space 7-11">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>  
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space3.jpg"  data-fancybox="images-preview" title="Working Space 12-14">
                <img src="img/working-space3.jpg" class="width100 opacity-hover" alt="Working Space 12-14" title="Working Space 12-14">
            </a>  
            <p class="four-img-p">Lounge</p>  	
        </div> 
        <div class="four-img-div">
            <a href="./img/working-space4.jpg"  data-fancybox="images-preview" title="Working Space 15-18">
                <img src="img/working-space4.jpg" class="width100 opacity-hover" alt="Working Space 15-18" title="Working Space 15-18">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>                           
	</div>
    <h2 class="backend-title-h2"> </h2> 
    <h2 class="backend-title-h2"> </h2> 
    <div class="big-container-for-seat">
    	<div class="eight-checkbox two-checkbox">
            <label class="container1"> Empty
              <input type="checkbox"   disabled>
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox two-checkbox">
            <label class="container1"> Occupied
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>                
    </div>    
    
	<h2 class="backend-title-h2">Empty Seat/Desk (2)</h2>    
    <div class="big-container-for-seat">
    	<div class="eight-checkbox">
            <label class="container1"> 1
              <input type="checkbox"   disabled>
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 2
              <input type="checkbox"  disabled>
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 3
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 4
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 5
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 6
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 7
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 8
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 9
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 10
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 11
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 12
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 13
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 14
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 15
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 16
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 17
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 18
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>                                                                                 
    </div>
    <div class="clear"></div>
	<h2 class="backend-title-h2">Empty Work Stations (0)</h2>    
    <div class="big-container-for-seat">
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 1
              <input type="checkbox"   disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 2
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 3
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 4
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 5
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 6
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>
    	<div class="eight-checkbox eight-checkbox2">
            <label class="container1"> 7
              <input type="checkbox"  disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>  
   </div>
    
    

</div>


<?php include 'js.php'; ?>
</body>
</html>