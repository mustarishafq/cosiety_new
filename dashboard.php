<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$currentDate = date("Y-m-d");

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userBooking = getBooking($conn," WHERE uid = ? AND payment_verify = 'APPROVED' ",array("uid"),array($uid),"s");
$userBookingDetails = $userBooking[0];

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Dashboard | Cosiety" />
<title>Dashboard | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
    <h1 class="backend-title-h1">Dashboard</h1>

    <h4>Welcome, <?php echo $userDetails->getUsername();?> </h4>

    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/notification.png" class="header-icon" alt="Notification/News" title="Notification/News"> <p>Notification/News</p>
                <a href="" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">

            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/booking.png" class="header-icon" alt="Booking" title="Booking"> <p>Upcoming Booking</p>
                <a href="booking.php" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
              <?php if ($userBooking) {
                for ($cnt=0; $cnt < count($userBooking) ; $cnt++) {
                   if ($currentDate < $userBooking[$cnt]->getStartDate()) {
                      if ($currentDate == date("Y-m-d", strtotime($userBooking[$cnt]->getStartDate())) || $currentDate <= date("Y-m-d", strtotime($userBooking[$cnt]->getEndDate()))) {
                        if ($currentDate == date("Y-m-d", strtotime($userBooking[$cnt]->getStartDate()))){
                          $startDate = "Today";
                        }
                        ?><div class="p-container">
															<div class="left-content-p">
																<p class="light-grey-text small-date hover-effect"><?php echo date("d-m-Y", strtotime($userBooking[$cnt]->getStartDate())) ?> - <?php echo date("d-m-Y", strtotime($userBooking[$cnt]->getEndDate())) ?><?php if($startDate){ ?><span class="green-status"> (TODAY)</span><?php } ?></p>
																<p class="white-box-content-p hover-effect"><?php echo $userBooking[$cnt]->getAreaType() ?></p>
															</div>
															<form action="receiptBookingHistory.php" method="POST" class="right-content-p">
																	<button class="clean receipt-btn" type="submit" name="bookingHistory_ID" value="<?php echo $userBooking[$cnt]->getBookingId();?>">
																			<img src="img/receipt.png" class="hover-effect receipt-img" alt="View Receipt" title="View Receipt">
																	</button>
															</form>
													</div><?php
                      }

                }

              }
            }
                ?>
            </div>
        </div>
    </div>

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->

        <div class="two-box-div overflow image-container">
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingPrivate.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>


</div>


<?php include 'js.php'; ?>
</body>
</html>
