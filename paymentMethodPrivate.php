<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Booking.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/classes/BookingPrivate.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$numberOfCheckBox = 74;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $title = rewrite($_POST["title"]);
    $duration = rewrite($_POST["duration"]);
    $start_date = rewrite($_POST["start_date"]);
    $discount = rewrite($_POST["discount"]);
    $total_price = rewrite($_POST["total_price"]);
    $totalPeople = rewrite($_POST["total_people"]);
    $timeLine = rewrite($_POST["timeline"]);

    // $area_type = rewrite($_POST["area_type"]);
    // $project_title = rewrite($_POST["project_title"]);
    // $project_details = rewrite($_POST["project_details"]);

}

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Method | Cosiety" />
<title>Payment Method | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">

<!-- <form action="utilities/bookingFunction.php" method="POST"> -->
<form name="checkboxLimited" action="utilities/bookingFunction.php" method="POST" enctype="multipart/form-data">


<input type="hidden" name="title" value="<?php echo $title ?>">
<input type="hidden" name="timeline" value="<?php echo $timeLine ?>">

<!-- <h4> <?php //echo $duration?> </h4>
<h4> <?php //echo $start_date?> </h4>
<h4> <?php //echo $discount?> </h4>
<h4> <?php //echo $total_price?> </h4>
<h4> <?php //echo $ids?> </h4>
<h4> <?php //echo $project_title?> </h4>
<h4> <?php //echo $project_details?> </h4> -->

<!-- <h4> <?php //echo $userDetails->getUsername();?> </h4> -->





    <h2 class="backend-title-h2">Floor Plan (<?php echo $title?> - Straits Quay)</h2>
       <a href="./img/zone3.jpg"  data-fancybox="images-preview" title="Floor Plan">
        <img src="img/zone3.png" class="floorplan-img" alt="Floor Plan" title="Click to Enlarge">
      </a>
    <h2 class="backend-title-h2">Choose your seat</h2>

    <div class="big-container-for-seat">
      <?php
        $conn = connDB();
        $bookingDetails = getPrivate($conn);

        // if($bookingDetails)
        // {
             $number = 1;
            for($cnt = 0;$cnt < $numberOfCheckBox ;$cnt++)
            {
              $currentDate = date("Y-m-d");
              $seat = $number++;

              $bookDetails = getPrivate($conn, "WHERE seat_id =? ", array("seat_id"), array($seat), "i");

              if ($bookDetails) {

                $startDate = $bookDetails[0]->getStartDate();
                $endDate = $bookDetails[0]->getEndDate();
                $startDates = date("Y-m-d", strtotime($startDate));
                $endDates = date("Y-m-d", strtotime($endDate));
                if ($start_date < $startDates || $start_date > $endDates) {

                  ?>

                  <div class="eight-checkbox">
                        <label class="container1"> <?php echo $seat; ?>
                          <input type="checkbox" name="seat_id[]" value="<?php echo $seat; ?>" >
                          <span class="checkmark1"></span>
                        </label>
                    </div>

                    <?php

                }else {
                  ?>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                  <div id="divtoshow" style="position: fixed;display:none;"><?php echo date("d-m-Y", strtotime($startDates)). " to " .date("d-m-Y", strtotime($endDates)) ?></div>

                  <div class="eight-checkbox">
                        <label class="container1"> <?php echo $seat; ?>
                          <input type="checkbox" disabled name="seat_id[]" value="<?php echo $seat; ?>">
                          <span class="checkmark1 booked" onmouseover="hoverdiv(event,'divtoshow')" onmouseout="hoverdiv(event,'divtoshow')"></span>
                        </label>
                    </div>

                    <?php
                }


              }else {
                ?>

                <div class="eight-checkbox">
                      <label class="container1"> <?php echo $seat; ?>
                        <input type="checkbox" name="seat_id[]" value="<?php echo $seat ?>">
                        <span class="checkmark1"></span>
                      </label>
                  </div>

                  <?php
              }



              if(!empty($_POST['seat_id']))
                {
                  foreach($_POST['seat_id'] as $check)
                    {
                      echo $check; //echoes the value set in the HTML form for each checked checkbox.
                      //so, if I were to check 1, 3, and 5 it would echo value 1, value 3, value 5.
                      //in your case, it would echo whatever $row['Report ID'] is equivalent to.
                  }
                }
              ?>

            <?php
            }
        // }
        $conn->close();
      ?>
    </div>

    <div class="three-div">
      <p class="grey-text input-top-p">Duration</p>
      <p class="three-select-p"><?php echo $duration; ?> Month</p>
      <input type="hidden" name="duration" value="<?php echo $duration; ?>">
    </div>

    <div class="three-div">
      <p class="grey-text input-top-p">Start Date</p>
      <p class="three-select-p"><?php echo $start_date ?></p>
      <input type="hidden" name="date" value="<?php echo $start_date ?>">
      <!-- <input type="date" class="three-select clean"> -->
    </div>

    <div class="three-div">
      <p class="grey-text input-top-p">Discount</p>
      <p class="three-select-p"><?php echo $discount; ?>%</p>
      <input type="hidden" name="discount" id="discount" value="<?php echo $discount; ?>">
    </div>

    <div class="three-div">
      <p class="grey-text input-top-p">Total Seat</p>
      <p class="three-select-p"><?php echo $totalPeople; ?></p>
      <input type="hidden" name="total_people" id="total_people" value="<?php echo $totalPeople; ?>">
    </div>

    <!-- <div class="tempo-three-clear"></div> -->

    <div class="three-div second-three-div">
      <p class="grey-text input-top-p">Total</p>
      <!-- <p class="total-p">RM<?php //$priceDiscount = $total_price * $duration * $totalPeople * ($discount/100);
              //$totalPrice = $total_price * $duration * $totalPeople; echo $totalPrice - $priceDiscount;?>
      </p>
      <input type="hidden" name="total_price" id="total_price" value="<?php//$priceDiscount = $total_price * $duration * $totalPeople * ($discount/100);
              //$totalPrice = $total_price * $duration * $totalPeople ; echo $totalPrice - $priceDiscount;?>">
      <input type="hidden" name="cost" id="cost" value="<?php //echo $price = $total_price * $duration * $totalPeople ; ?>"> -->

      <p class="total-p">RM<?php $priceDiscount = $total_price * $duration * ($discount/100);
              $totalPrice = $total_price * $duration; echo $totalPrice - $priceDiscount;?>
      </p>
      <input type="hidden" name="total_price" id="total_price" value="<?php $priceDiscount = $total_price * $duration * ($discount/100);
              $totalPrice = $total_price * $duration; echo $totalPrice - $priceDiscount;?>">
      <input type="hidden" name="cost" id="cost" value="<?php echo $price = $total_price * $duration; ?>">

    </div>

    <div class="tempo-three-clear"></div>

    <!-- <h2 class="backend-title-h2">Choose Your Payment Method</h2> -->


      <h2 class="backend-title-h2">Choose Your Payment Method</h2>

      <div class="three-div">
          <select class="three-select clean" id="payment_method" name="payment_method" type = "text" onchange = "ShowHideDiv()">
              <!-- <option value="ipay88" name="ipay88">ipay88</option> -->
              <!-- <option value="Visa or MasterCard" name="Visa or MasterCard">Visa or MasterCard</option> -->
              <option value="">**Select an option</option>
              <option value="Online Banking" name="Online Banking">Online Banking</option>
          </select>
      </div>
      <div class="clear"></div><br>
      <div id="onlineBanking" style="display: none">
        <div class="three-div">
            <p class="grey-text input-top-p">Bank : MAYBANK</p>
            <p class="grey-text input-top-p">Acc. No: XXXXXXXXXXXXXX</p>
            <p class="grey-text input-top-p">Bank Acc. Holder: Cosiety</p>
        </div>

    <div class="clear"></div>

    <div class="three-div">
      <p class="grey-text input-top-p">Bank In Amount (RM)</p>
      <input type="text" class="three-select clean " id="bank_in_amount" name="bank_in_amount" placeholder="Amount">
    </div>

    <div class="clear"></div>

    <div class="three-div">
      <p class="grey-text input-top-p">Date and Time</p>
      <input type="datetime-local" class="three-select clean " id="date_time" name="date_time">
    </div>

    <div class="clear"></div>

    <div class="three-div">
      <p class="grey-text input-top-p">Bank In Reference</p>
      <input type="text" class="three-select clean " id="bank_in_reference" name="bank_in_reference" placeholder="Reference">
    </div>

    <div class="clear"></div>

    <div class="three-div">
      <p class="grey-text input-top-p">Upload Receipt:&nbsp;
      <!-- <button class="upload-btn">Upload Receipt</button> -->
      <input class="hidden-input" type="file" name="file" />
      </p>
    </div>
        </div>

    <div class="three-div">
      <!-- <p class="grey-text input-top-p">Price (RM)</p> -->
        <input type="hidden" name="price" readonly value="<?php echo $total_price; ?>">
    </div>

    <div class="clear"></div>

      <div class="three-div">
        	<!-- <p class="grey-text input-top-p">Bank Reference :</p> -->
            <!-- <input type="text" name="bank_reference"> -->
            <input type="hidden" name="seat" readonly value="<?php echo $seat_id; ?>">
            <input type="hidden" name="date" readonly value="<?php echo $start_date; ?>">
            <input type="hidden" name="duration" value="<?php echo $duration; ?>">
            <input type="hidden" name="user" value="<?php echo $uid ?>">
            <input type="hidden" name="title" value="<?php echo $title ?>">
            <!-- <input type="hidden" name="seat_testing3" value="<?php //echo $seat_testing ?>"> -->
        </div>
      <div class="clear"></div>
  	<div class="divider"></div>

    <!-- <h2 class="backend-title-h2">Choose Your Payment Method</h2>
    <div class="three-div-radio">
        <label class="container2"><img src="img/ipay88.png" class="payment-img" alt="ipay88" title="ipay88">
          <input type="radio" id="ipay88" name="ipay88">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="three-div-radio">
        <label class="container2"><img src="img/visa-mastercard.png" class="payment-img" alt="Visa/Master Card" title="Visa/Master Card">
          <input type="radio" id="Visa_or_MasterCard" name="Visa_or_MasterCard">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="three-div-radio no-margin-right">
        <label class="container2">Online Banking
          <input type="radio" id="online_banking" name="online_banking">
          <span class="checkmark2"></span>
        </label>
    </div> -->

    <div class="divider"></div>
    <div class="clear"></div>

    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean next-btn" type="submit" name="updateButtonPrivate" >Pay</button>
    <!-- <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a> -->
    <div class="clear"></div>
	<div class="divider"></div>
</form>
</div>

<script type="text/javascript">checkBoxLimit()
function checkBoxLimit()
{
	var checkBoxGroup = document.forms['checkboxLimited']['seat_id[]'];
  var val = "<?php echo $totalPeople ?>";
	var limit = val;
	for (var i = 0; i < checkBoxGroup.length; i++)
  {
		checkBoxGroup[i].onclick = function()
    {
			var checkedcount = 0;
			for (var i = 0; i < checkBoxGroup.length; i++) {
				checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
			}
			if (checkedcount > limit)
      {
				console.log("You can select maximum of " + limit + " Seat.");
				alert("You can select maximum of " + limit + " Seat.");
				this.checked = false;
			}
		}
	}
}
</script>
<script type="text/javascript">
    function ShowHideDiv() {
        var paymentMethod = document.getElementById("payment_method");
        //var iPay = document.getElementById("iPay");
        var onlineBanking = document.getElementById("onlineBanking");
        onlineBanking.style.display = paymentMethod.value == "Online Banking" ? "block" : "none";
        //iPay.style.display = paymentMethod.value == "iPay" ? "block" : "none";
    }
</script>
<script type="text/javascript">
  function hoverdiv(e,divid){

  var left  = e.clientX  + "px";
  var top  = e.clientY  + "px";

  var div = document.getElementById(divid);

  div.style.left = left;
  div.style.top = top;

  $("#"+divid).toggle();
  return false;
}
</script>

<?php include 'js.php'; ?>
</body>
</html>
