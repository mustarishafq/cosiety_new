<?php
if (session_id() == ""){
     session_start();
 }
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$username,$uid,$email,$finalPassword,$salt,$country,$phoneNo){
 
 
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","country","phone_no"),
         array($uid,$username,$email,$finalPassword,$salt,$country,$phoneNo),"sssssss") === null)
          {
               // return false;
          }
          else
          {
               //echo "bbbb";
          }
 
     return true;
 }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());
     $register_username = rewrite($_POST['register_username']);
     $register_email_user = rewrite($_POST['register_email_user']);
     $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];


     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $register_country = $_POST['register_country'];
     $register_contact = $_POST['register_contact'];

          //   FOR DEBUGGING 
          // echo $register_uid;
          // echo $register_username."<br>";
          // echo $register_email_user."<br>";
          // echo $register_password."<br>";
          // echo $register_retype_password."<br>";
          // echo $register_country."<br>";
          // echo $register_contact."<br>";


     if(filter_var($register_email_user, FILTER_VALIDATE_EMAIL))
     {
          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    // echo $register_uid."<br>";
                    // echo $register_username."<br>";
                    // echo $register_email_user."<br>";
                    // echo $finalPassword."<br>";
                    // echo $salt."<br>";

                    if(registerNewUser($conn,$register_username,$register_uid,$register_email_user,$finalPassword,$salt,$register_country,$register_contact))
                    {
                         // $_SESSION['messageType'] = 1;
                         // header('Location: ../index.php?type=1');
                         // echo "register success";
                         header('Location: ../index.php');
                    }
               }
          
          }     
          else 
          {

          }
     }else{
          // $_SESSION['messageType'] = 1;
          // header('Location: ../index.php?type=1')
          echo "wrong email format";
     }
    
}
else 
{
     // header('Location: ../index.php');
     echo "register fail";
}
?>