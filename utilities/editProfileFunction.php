<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $username = rewrite($_POST["username"]);
        $fullname = rewrite($_POST["update_fullname"]);
        $country = rewrite($_POST["update_country"]);
        $contact = rewrite($_POST["update_contact"]);
        $gender = rewrite($_POST["update_gender"]);
        $birthday = rewrite($_POST["update_birthday"]);

        //   FOR DEBUGGING

        // echo "<br>";
        // echo $username."<br>";
        // echo $fullname."<br>";
        // echo $country."<br>";
        // echo $contact."<br>";
        // echo $gender."<br>";
        // echo $birthday."<br>";
        
        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");    

        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($fullname)
            {
                array_push($tableName,"full_name");
                array_push($tableValue,$fullname);
                $stringType .=  "s";
            }
            if($country)
            {
                array_push($tableName,"country");
                array_push($tableValue,$country);
                $stringType .=  "s";
            }
            if($contact)
            {
                array_push($tableName,"phone_no");
                array_push($tableValue,$contact);
                $stringType .=  "s";
            }
            if($gender)
            {
                array_push($tableName,"gender");
                array_push($tableValue,$gender);
                $stringType .=  "s";
            }
            if($birthday)
            {
                array_push($tableName,"birthday");
                array_push($tableValue,$birthday);
                $stringType .=  "s";
            }




            array_push($tableValue,$uid);
            $stringType .=  "s";

            // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            // if($passwordUpdated)

            $profileUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($profileUpdated)
            {
                // echo "success";
                $_SESSION['messageType'] = 1;
                header('Location: ../profile.php?type=1');
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../editProfile.php?type=2');
            }
        }
        else
        {
            // echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=3');
        }

    }
else 
{
    // echo "404";
    header('Location: ../index.php');
}
?>
