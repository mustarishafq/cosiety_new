<?php
// if (session_id() == "")
// {
//      session_start();
// }
require_once dirname(__FILE__) . '../../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Booking.php';
require_once dirname(__FILE__) . '/../classes/BookingWorkDesk.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


function registerNewBooking($conn,$uid,$bookingId,$title,$cost,$duration,$date,$endDate,$totalPeople,$userName,
                                // $discount,$totalPrice,$totalSeat,$projectTitle,$projectDetails)
                                $discount,$price,$totalSeat,$projectTitle,$projectDetails,$paymentMethod,$timeLine,$paymentAmount,$payment_time,$paymentVerify,$receiptName)
{
    if(insertDynamicData($conn,"booking",
            array("uid","booking_id","area_type","cost","duration","start_date","end_date","total_ppl","order_by",
                        // "discount","total_price","total_seat","project_title","project_details"),
                    "discount","total_price","total_seat","project_title","project_details","payment_method","timeline","payment_amount","payment_time", "payment_verify","receipt"),
            array($uid,$bookingId,$title,$cost,$duration,$date,$endDate,$totalPeople,$userName,
                    //$discount,$totalPrice,$totalSeat,$projectTitle,$projectDetails),"sssssssssssss") === null)
                    $discount,$price,$totalSeat,$projectTitle,$projectDetails,$paymentMethod,$timeLine,$paymentAmount,$payment_time,$paymentVerify,$receiptName),"sssdssssssdsssssdsss") === null)
    {
    // echo $finalPassword;
    }
    else
    {
    // echo "bbbb";
    }
return true;
}

function registerPayment($conn,$uid,$userName,$receiptName)
{
    if(insertDynamicData($conn,"payment",
            array("uid","username","receipt"),
            array($uid,$userName,$receiptName),"sss") === null)
    {
    // echo $finalPassword;
    }
    else
    {
    // echo "bbbb";
    }
return true;
}
function bookingPrivate($conn,$selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName)
{
    if(insertDynamicData($conn,"booking_private",
            array("seat_id","booking_id","qr_code_id","seat_status","start_date","duration","end_date","payment_amount","payment_verify","orderBy"),
            array($selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName),"issisisdsss") === null)
    {
    // echo $finalPassword;
    }
    else
    {
    // echo "bbbb";
    }
return true;
}
function bookingWorkdesk($conn,$selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName)
{
    if(insertDynamicData($conn,"booking_workdesk",
            array("seat_id","booking_id","qr_code_id","seat_status","start_date","duration","end_date","payment_amount","payment_time","payment_verify","orderBy"),
            array($selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName),"issisisdsss") === null)
    {
    // echo $finalPassword;
    }
    else
    {
    // echo "bbbb";
    }
return true;
}




if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     //$uid = rewrite($_POST["uid"]);
     $id = md5(uniqid());
     $bookingId = md5(uniqid());

     $cost = rewrite($_POST["cost"]);
     $totalPeople = rewrite($_POST["total_people"]);
     $seatStatus = 0;
     $paymentVerify = "PENDING";
      $payment_time = rewrite($_POST["date_time"]);
      $paymentAmount = rewrite($_POST["bank_in_amount"]);
     //$projectTitle = rewrite($_POST["project_title"]);
     //$projectDetails = rewrite($_POST["project_details"]);
     $paymentMethod = rewrite($_POST["payment_method"]);
     $uid = rewrite($_POST["user"]);
     $title = rewrite($_POST["title"]);
     $seat = rewrite($_POST['seat']);
     $discount = rewrite($_POST['discount']);
     $user = rewrite($_POST['user']);
     $price = rewrite($_POST['total_price']);
     $date = rewrite($_POST['date']);
     //$receiptName = rewrite($_POST['file']);
     $duration = rewrite($_POST['duration']);
     $seatID = rewrite($_POST['seat_id']);
     $totalSeat = count($_POST['seat_id']);
     $timeLine = rewrite($_POST["timeline"]);
     $space = " ";
     $time = $duration . $space . $timeLine;
     $endDate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($date))."+ $time"));
   }

   $receiptName = $_FILES['file']['name'];
   $target_dir = "../receipt/";
   $target_file = $target_dir . basename($_FILES["file"]["name"]);

   $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

   // Valid file extensions
   $extensions_arr = array("jpg","jpeg","png","gif");

   if( in_array($imageFileType,$extensions_arr) ){


    move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$receiptName);
  }


          //   FOR DEBUGGING
          // echo $register_uid;

          if ($price) {
            $available = 1;
          }

    // $roomDetails = getWorkDesk($conn, "WHERE seat_id =?", array("seat_id"), array($seat), "i");
    // $seatDetails = $roomDetails[0]->getSeatID();

    $userDetails = getUser($conn, "WHERE uid =?", array("uid"), array($uid), "s");
    $userName = $userDetails[0]->getUsername();


    if(!empty($_POST['seat_id']))
    {

        foreach(array_slice($_POST['seat_id'],0,$totalPeople) as $selected)
        //foreach($_POST['seat_id'] as $selected)
            {
                 echo "<p>".$selected ."</p>";
                 $newBookingID = $bookingId . $selected;

                 if(isset($_POST["updateButton"])){

                   if(bookingWorkdesk($conn,$selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName))
                   {
                           // $_SESSION['messageType'] = 1;
                           // header('Location: ../index.php?type=1');
                           //echo "register success";
                           header('Location: ../addBooking.php?type=1');
                   }

                 }
                 if (isset($_POST["updateButtonPrivate"])) {
                   if(bookingPrivate($conn,$selected,$bookingId,$newBookingID,$seatStatus,$date,$duration,$endDate,$price,$payment_time,$paymentVerify,$userName))
                   {
                           // $_SESSION['messageType'] = 1;
                           // header('Location: ../index.php?type=1');
                           //echo "register success";
                           header('Location: ../addBooking.php?type=1');
                   }
                 }

            }

            if (isset($_POST["updateButton"])) {
              if(registerNewBooking($conn,$uid,$bookingId,$title,$cost,$duration,$date,$endDate,$totalPeople,$userName,
                          // $discount,$totalPrice,$totalSeat,$projectTitle,$projectDetails))
                          $discount,$price,$totalSeat,$projectTitle,$projectDetails,$paymentMethod,$timeLine,$paymentAmount,$payment_time,$paymentVerify,$receiptName))
              {
                      // $_SESSION['messageType'] = 1;
                      // header('Location: ../index.php?type=1');
                      //echo "register success";
                      header('Location: ../addBooking.php?type=1');

              }

              if(registerPayment($conn,$uid,$userName,$receiptName))
              {
                      // $_SESSION['messageType'] = 1;
                      // header('Location: ../index.php?type=1');
                      //echo "register success";
                      header('Location: ../addBooking.php?type=1');
              }
            }
            if (isset($_POST["updateButtonPrivate"])) {
              if(registerNewBooking($conn,$uid,$bookingId,$title,$cost,$duration,$date,$endDate,$totalPeople,$userName,
                          // $discount,$totalPrice,$totalSeat,$projectTitle,$projectDetails))
                          $discount,$price,$totalSeat,$projectTitle,$projectDetails,$paymentMethod,$timeLine,$paymentAmount,$payment_time,$paymentVerify,$receiptName))
              {
                      // $_SESSION['messageType'] = 1;
                      // header('Location: ../index.php?type=1');
                      //echo "register success";
                      header('Location: ../addBooking.php?type=1');
              }
              if(registerPayment($conn,$uid,$userName,$receiptName))
              {
                      // $_SESSION['messageType'] = 1;
                      // header('Location: ../index.php?type=1');
                      //echo "register success";
                      header('Location: ../addBooking.php?type=1');
              }
            }
    }



?>
