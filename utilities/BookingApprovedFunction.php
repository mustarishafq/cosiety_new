<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Booking.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $paymentVerify = "APPROVED";
        $order_id = rewrite($_POST["booking_id"]);

        //for debugging
        // echo "<br>";
        // echo $_POST['order_id']."<br>";

        if(isset($_POST['booking_id']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($paymentVerify)
            {
                array_push($tableName,"payment_verify");
                array_push($tableValue,$paymentVerify);
                $stringType .=  "s";
            }     
            
            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"booking"," WHERE booking_id = ? ",$tableName,$tableValue,$stringType);
            
            if($orderUpdated)
            {
                // echo "<br>";
                // echo "success";
                // $_SESSION['messageType'] = 1;
                header('Location: ../outstandingPlan.php');
            }
            else
            {
                echo "fail";
            }
        }
        else
        {
            echo "dunno";
        }

    }
else 
{
    header('Location: ../index.php');
}

?>