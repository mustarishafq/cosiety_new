<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Promotion | Cosiety" />
<title>Promotion | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Promotion <a href="addPromotion.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add New Issue" title="Add New Issue"><img src="img/add2.png" class="add-icon hover1b" alt="Add Promotion" title="Add Promotion"></a></h1>
	<div class="clear"></div>
    <div class="width100">
		<div class="promo-half-div">
        	<h3 class="promo-h3">Promotion 1</h3>
         
            <a href="editPromotion.php"><img src="img/edit-button.png" class="hover-effect edit-icon"></a>
            
            <div class="clear"></div>
            <img src="img/promotion.jpg" class="width100">
        </div>
		<div class="promo-half-div second-promo-half-div">
         	<h3 class="promo-h3">Promotion 2</h3>
           
            <a href="editPromotion.php"><img src="img/edit-button.png" class="hover-effect edit-icon"></a>
            
            <div class="clear"></div>
            <img src="img/promotion.jpg" class="width100">       
        </div>         
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>